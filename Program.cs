﻿using System;
using System.Collections.Generic;

namespace ChaosArray
{
    class Program
    {
        private static ChaosList<int> myList = new ChaosList<int>();
        static void Main(string[] args)
        {
            for (int i = 0; i<5; i++)
            {
                myList.Add(i);
            }
            Console.WriteLine(myList.ToString());
            try{
                Console.WriteLine(myList.Get());
                Console.WriteLine(myList.Get());
                Console.WriteLine(myList.Get());
                Console.WriteLine(myList.Get());
                Console.WriteLine(myList.Get());
                Console.WriteLine(myList.Get());
                Console.WriteLine(myList.Get());
            }
            catch (ArrayIsEmptyException e)
            {
                Console.WriteLine("Cannot get values, List is empty.");
            }

        }
    }
}
