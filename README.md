## Chaosarray

Randomized array made using lists, with only two methods for access: Add and Get.

* Add method that adds to a random position in the list.
* Get method returns a random element from the list. 

Error catching implemented when trying to Get from an empty array. Add will never cause an error in this implementation, hence no errors were made for that. 