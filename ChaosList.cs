﻿using System.Collections.Generic;
using System;
namespace ChaosArray
{
    public class ChaosList<T>
    {
        private List<T> myList;
        public ChaosList()
        {
            myList = new List<T>();
        }
        public void Add(T input) {
            var random = new Random();
            int index = random.Next(myList.Count);
            myList.Insert(index, input);
        }
        public T Get(){
            if (myList.Count == 0) throw new ArrayIsEmptyException();
            var random = new Random();
            int index = random.Next(myList.Count);
            T item = myList[index];
            myList.RemoveAt(index);
            return item;
        }
        public override string ToString()
        {
            string array="";
            foreach (T item in myList)
            {
                array += item + " ";
            }
            return array;
        }
    }
    public class ItemExistsInIndex : Exception
    {
        public ItemExistsInIndex()
        {
        }

        public ItemExistsInIndex(int index)
            : base("Index at " + index + "already occupied")
        {
        }
    }
    public class ArrayIsEmptyException : Exception
    {
        public ArrayIsEmptyException()
        {
        }

        //public ArrayIsEmpty(int index)
        //    : base("Nothing at index " + index + "!")
        //{
        //}
    }
}